(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Handlebars.registerHelper('img', function(src) {
  return cordova.file.applicationDirectory + 'www/img/' + src;
})

},{}],2:[function(require,module,exports){
require('./helpers');

var appChannel = Backbone.Radio.channel('app');

var Layout = require('./layout');
var Router = require('./router');

document.addEventListener('deviceready', onDeviceReady, false);

window.App = new Marionette.Application({
  onStart: function () {
    this.router = new Router();
    // Backbone.Intercept.start();
    Backbone.history.start({
      pushState: true
    });
    layout = new Layout();
    Backbone.history.navigate('/login', {trigger: true});
  }
});

function onDeviceReady() {
  App.start();
}

},{"./helpers":1,"./layout":3,"./router":5}],3:[function(require,module,exports){
var layoutChannel = Backbone.Radio.channel('layout');
var template = require('./template.hbs');

var Layout = Marionette.LayoutView.extend({
  template: template,
  el: 'body',
  regions: {
    header: 'header',
    main: 'main',
    footer: 'footer',
    modal: '#modal',
    notifications: '#notifications'
  },
  initialize: function() {
    var self = this;
    this.render();
    layoutChannel.reply('self', function() {
      return self;
    })
  }
});


module.exports = Layout;

},{"./template.hbs":4}],4:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = Handlebars;
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<header class=\"app-header\"></header>\n<main></main>\n<footer></footer>\n<div id=\"modal\"></div>\n<div id=\"notifications\"></div>\n";
},"useData":true});

},{}],5:[function(require,module,exports){
var layout = Backbone.Radio.channel('layout');

var Login = require('./login');

var Router = Marionette.AppRouter.extend({
  appRoutes: {
    login: 'login',
  },

  controller: {
    login: function() {
      layout.request('self').getRegion('main').show(new Login());
    },
  },
});

module.exports = Router;

},{"./login":6}],6:[function(require,module,exports){
var template = require('./template.hbs');

var Login = Marionette.ItemView.extend({
  className: 'col-md-4 col-md-offset-4 m-t-lg',
  template: template,
  events: {
    'click #signIn': 'onSign',
  },
  onSign: function(e) {
    e.preventDefault();
    var missing = '';
    var email = $('#email').val();
    var password = $('#password').val();
    if (!email) missing += ' correo';
    if (!password) missing += ' contrase&oacute;a';
    if (missing) {
      return alert("Porfavor revisa los siguientes campos: " + missing);
    }
  },
});


module.exports = Login;

},{"./template.hbs":7}],7:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = Handlebars;
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<section class=\"panel\">\n  <header class=\"panel-heading text-center\"></header>\n  <div class=\"panel-body\">\n    <div class=\"form-group\">\n    <label class=\"control-label\">Correo Electr&oacute;nico</label>\n      <input type=\"email\" placeholder=\"nombre@mail.com\" class=\"form-control\" id=\"email\">\n    </div>\n    <div class=\"form-group\">\n      <label class=\"control-label\">Contrase&ntilde;a</label>\n      <input type=\"password\" id=\"password\" placeholder=\"Contrase&ntilde;a\" class=\"form-control\">\n    </div>\n    <button id=\"signIn\" class=\"btn btn-info\">Entrar</button>\n    <div class=\"line line-dashed\"></div>\n  </div>\n</section>\n";
},"useData":true});

},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9oZWxwZXJzLmpzIiwic3JjL3NjcmlwdHMvaW5kZXguanMiLCJzcmMvc2NyaXB0cy9sYXlvdXQvaW5kZXguanMiLCJzcmMvc2NyaXB0cy9sYXlvdXQvdGVtcGxhdGUuaGJzIiwic3JjL3NjcmlwdHMvcm91dGVyL2luZGV4LmpzIiwic3JjL3NjcmlwdHMvcm91dGVyL2xvZ2luL2luZGV4LmpzIiwic3JjL3NjcmlwdHMvcm91dGVyL2xvZ2luL3RlbXBsYXRlLmhicyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBOztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiSGFuZGxlYmFycy5yZWdpc3RlckhlbHBlcignaW1nJywgZnVuY3Rpb24oc3JjKSB7XG4gIHJldHVybiBjb3Jkb3ZhLmZpbGUuYXBwbGljYXRpb25EaXJlY3RvcnkgKyAnd3d3L2ltZy8nICsgc3JjO1xufSlcbiIsInJlcXVpcmUoJy4vaGVscGVycycpO1xuXG52YXIgYXBwQ2hhbm5lbCA9IEJhY2tib25lLlJhZGlvLmNoYW5uZWwoJ2FwcCcpO1xuXG52YXIgTGF5b3V0ID0gcmVxdWlyZSgnLi9sYXlvdXQnKTtcbnZhciBSb3V0ZXIgPSByZXF1aXJlKCcuL3JvdXRlcicpO1xuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdkZXZpY2VyZWFkeScsIG9uRGV2aWNlUmVhZHksIGZhbHNlKTtcblxud2luZG93LkFwcCA9IG5ldyBNYXJpb25ldHRlLkFwcGxpY2F0aW9uKHtcbiAgb25TdGFydDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICAgIC8vIEJhY2tib25lLkludGVyY2VwdC5zdGFydCgpO1xuICAgIEJhY2tib25lLmhpc3Rvcnkuc3RhcnQoe1xuICAgICAgcHVzaFN0YXRlOiB0cnVlXG4gICAgfSk7XG4gICAgbGF5b3V0ID0gbmV3IExheW91dCgpO1xuICAgIEJhY2tib25lLmhpc3RvcnkubmF2aWdhdGUoJy9sb2dpbicsIHt0cmlnZ2VyOiB0cnVlfSk7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBvbkRldmljZVJlYWR5KCkge1xuICBBcHAuc3RhcnQoKTtcbn1cbiIsInZhciBsYXlvdXRDaGFubmVsID0gQmFja2JvbmUuUmFkaW8uY2hhbm5lbCgnbGF5b3V0Jyk7XG52YXIgdGVtcGxhdGUgPSByZXF1aXJlKCcuL3RlbXBsYXRlLmhicycpO1xuXG52YXIgTGF5b3V0ID0gTWFyaW9uZXR0ZS5MYXlvdXRWaWV3LmV4dGVuZCh7XG4gIHRlbXBsYXRlOiB0ZW1wbGF0ZSxcbiAgZWw6ICdib2R5JyxcbiAgcmVnaW9uczoge1xuICAgIGhlYWRlcjogJ2hlYWRlcicsXG4gICAgbWFpbjogJ21haW4nLFxuICAgIGZvb3RlcjogJ2Zvb3RlcicsXG4gICAgbW9kYWw6ICcjbW9kYWwnLFxuICAgIG5vdGlmaWNhdGlvbnM6ICcjbm90aWZpY2F0aW9ucydcbiAgfSxcbiAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMucmVuZGVyKCk7XG4gICAgbGF5b3V0Q2hhbm5lbC5yZXBseSgnc2VsZicsIGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHNlbGY7XG4gICAgfSlcbiAgfVxufSk7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBMYXlvdXQ7XG4iLCIvLyBoYnNmeSBjb21waWxlZCBIYW5kbGViYXJzIHRlbXBsYXRlXG52YXIgSGFuZGxlYmFyc0NvbXBpbGVyID0gSGFuZGxlYmFycztcbm1vZHVsZS5leHBvcnRzID0gSGFuZGxlYmFyc0NvbXBpbGVyLnRlbXBsYXRlKHtcImNvbXBpbGVyXCI6WzcsXCI+PSA0LjAuMFwiXSxcIm1haW5cIjpmdW5jdGlvbihjb250YWluZXIsZGVwdGgwLGhlbHBlcnMscGFydGlhbHMsZGF0YSkge1xuICAgIHJldHVybiBcIjxoZWFkZXIgY2xhc3M9XFxcImFwcC1oZWFkZXJcXFwiPjwvaGVhZGVyPlxcbjxtYWluPjwvbWFpbj5cXG48Zm9vdGVyPjwvZm9vdGVyPlxcbjxkaXYgaWQ9XFxcIm1vZGFsXFxcIj48L2Rpdj5cXG48ZGl2IGlkPVxcXCJub3RpZmljYXRpb25zXFxcIj48L2Rpdj5cXG5cIjtcbn0sXCJ1c2VEYXRhXCI6dHJ1ZX0pO1xuIiwidmFyIGxheW91dCA9IEJhY2tib25lLlJhZGlvLmNoYW5uZWwoJ2xheW91dCcpO1xuXG52YXIgTG9naW4gPSByZXF1aXJlKCcuL2xvZ2luJyk7XG5cbnZhciBSb3V0ZXIgPSBNYXJpb25ldHRlLkFwcFJvdXRlci5leHRlbmQoe1xuICBhcHBSb3V0ZXM6IHtcbiAgICBsb2dpbjogJ2xvZ2luJyxcbiAgfSxcblxuICBjb250cm9sbGVyOiB7XG4gICAgbG9naW46IGZ1bmN0aW9uKCkge1xuICAgICAgbGF5b3V0LnJlcXVlc3QoJ3NlbGYnKS5nZXRSZWdpb24oJ21haW4nKS5zaG93KG5ldyBMb2dpbigpKTtcbiAgICB9LFxuICB9LFxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gUm91dGVyO1xuIiwidmFyIHRlbXBsYXRlID0gcmVxdWlyZSgnLi90ZW1wbGF0ZS5oYnMnKTtcblxudmFyIExvZ2luID0gTWFyaW9uZXR0ZS5JdGVtVmlldy5leHRlbmQoe1xuICBjbGFzc05hbWU6ICdjb2wtbWQtNCBjb2wtbWQtb2Zmc2V0LTQgbS10LWxnJyxcbiAgdGVtcGxhdGU6IHRlbXBsYXRlLFxuICBldmVudHM6IHtcbiAgICAnY2xpY2sgI3NpZ25Jbic6ICdvblNpZ24nLFxuICB9LFxuICBvblNpZ246IGZ1bmN0aW9uKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdmFyIG1pc3NpbmcgPSAnJztcbiAgICB2YXIgZW1haWwgPSAkKCcjZW1haWwnKS52YWwoKTtcbiAgICB2YXIgcGFzc3dvcmQgPSAkKCcjcGFzc3dvcmQnKS52YWwoKTtcbiAgICBpZiAoIWVtYWlsKSBtaXNzaW5nICs9ICcgY29ycmVvJztcbiAgICBpZiAoIXBhc3N3b3JkKSBtaXNzaW5nICs9ICcgY29udHJhc2Umb2FjdXRlO2EnO1xuICAgIGlmIChtaXNzaW5nKSB7XG4gICAgICByZXR1cm4gYWxlcnQoXCJQb3JmYXZvciByZXZpc2EgbG9zIHNpZ3VpZW50ZXMgY2FtcG9zOiBcIiArIG1pc3NpbmcpO1xuICAgIH1cbiAgfSxcbn0pO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gTG9naW47XG4iLCIvLyBoYnNmeSBjb21waWxlZCBIYW5kbGViYXJzIHRlbXBsYXRlXG52YXIgSGFuZGxlYmFyc0NvbXBpbGVyID0gSGFuZGxlYmFycztcbm1vZHVsZS5leHBvcnRzID0gSGFuZGxlYmFyc0NvbXBpbGVyLnRlbXBsYXRlKHtcImNvbXBpbGVyXCI6WzcsXCI+PSA0LjAuMFwiXSxcIm1haW5cIjpmdW5jdGlvbihjb250YWluZXIsZGVwdGgwLGhlbHBlcnMscGFydGlhbHMsZGF0YSkge1xuICAgIHJldHVybiBcIjxzZWN0aW9uIGNsYXNzPVxcXCJwYW5lbFxcXCI+XFxuICA8aGVhZGVyIGNsYXNzPVxcXCJwYW5lbC1oZWFkaW5nIHRleHQtY2VudGVyXFxcIj48L2hlYWRlcj5cXG4gIDxkaXYgY2xhc3M9XFxcInBhbmVsLWJvZHlcXFwiPlxcbiAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG4gICAgPGxhYmVsIGNsYXNzPVxcXCJjb250cm9sLWxhYmVsXFxcIj5Db3JyZW8gRWxlY3RyJm9hY3V0ZTtuaWNvPC9sYWJlbD5cXG4gICAgICA8aW5wdXQgdHlwZT1cXFwiZW1haWxcXFwiIHBsYWNlaG9sZGVyPVxcXCJub21icmVAbWFpbC5jb21cXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIGlkPVxcXCJlbWFpbFxcXCI+XFxuICAgIDwvZGl2PlxcbiAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIj5cXG4gICAgICA8bGFiZWwgY2xhc3M9XFxcImNvbnRyb2wtbGFiZWxcXFwiPkNvbnRyYXNlJm50aWxkZTthPC9sYWJlbD5cXG4gICAgICA8aW5wdXQgdHlwZT1cXFwicGFzc3dvcmRcXFwiIGlkPVxcXCJwYXNzd29yZFxcXCIgcGxhY2Vob2xkZXI9XFxcIkNvbnRyYXNlJm50aWxkZTthXFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sXFxcIj5cXG4gICAgPC9kaXY+XFxuICAgIDxidXR0b24gaWQ9XFxcInNpZ25JblxcXCIgY2xhc3M9XFxcImJ0biBidG4taW5mb1xcXCI+RW50cmFyPC9idXR0b24+XFxuICAgIDxkaXYgY2xhc3M9XFxcImxpbmUgbGluZS1kYXNoZWRcXFwiPjwvZGl2PlxcbiAgPC9kaXY+XFxuPC9zZWN0aW9uPlxcblwiO1xufSxcInVzZURhdGFcIjp0cnVlfSk7XG4iXX0=
