require('./helpers');

var appChannel = Backbone.Radio.channel('app');

var Layout = require('./layout');
var Router = require('./router');

document.addEventListener('deviceready', onDeviceReady, false);

window.App = new Marionette.Application({
  onStart: function () {
    this.router = new Router();
    Backbone.Intercept.start();
    Backbone.history.start({
      pushState: true
    });
    layout = new Layout();
    // Backbone.history.navigate('/login', {trigger: true});
    // Backbone.history.navigate('/home', {trigger: true});
  }
});

function onDeviceReady() {
  App.start();
}
