var layout = Backbone.Radio.channel('layout');

var Login = require('./login');
var Home = require('./home');

var Router = Marionette.AppRouter.extend({
  appRoutes: {
    login: 'login',
    home: 'home',
  },

  controller: {
    login: function() {
      layout.request('self').getRegion('main').show(new Login());
    },
    home: function(){
      layout.request('self').getRegion('main').show(new Home());
    }
  },
});

module.exports = Router;
