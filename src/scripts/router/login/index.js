var template = require('./template.hbs');

var Login = Marionette.ItemView.extend({
  className: 'col-md-4 col-md-offset-4 m-t-lg',
  template: template,
  events: {
    'click #signIn': 'onSign',
  },
  onSign: function(e) {
    e.preventDefault();
    var missing = '';
    var email = $('#email').val();
    var password = $('#password').val();
    if (!email) missing += ' correo';
    if (!password) missing += ' contrase&oacute;a';
    if (missing) {
      return alert("Porfavor revisa los siguientes campos: " + missing);
    }
  },
});


module.exports = Login;
