var layoutChannel = Backbone.Radio.channel('layout');
var template = require('./template.hbs');

var Layout = Marionette.LayoutView.extend({
  template: template,
  el: 'body',
  regions: {
    header: 'header',
    main: 'main',
    footer: 'footer',
    modal: '#modal',
    notifications: '#notifications'
  },
  initialize: function() {
    var self = this;
    this.render();
    layoutChannel.reply('self', function() {
      return self;
    })
  }
});


module.exports = Layout;
