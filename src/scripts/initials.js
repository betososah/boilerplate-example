var w = window;


w.Collection = Backbone.Collection;
w.Router = Backbone.Router;
w.ItemView = Marionette.ItemView;
w.LayoutView = Marionette.LayoutView;
w.CollectionView = Marionette.CollectionView;
w.CompositeView = Marionette.CompositeView;
w.AppRouter = Marionette.AppRouter;
